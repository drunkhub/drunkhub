# DrunkHUB
[![pipeline status](https://gitlab.com/drunkhub/drunkhub/badges/master/pipeline.svg)](https://gitlab.com/drunkhub/drunkhub/-/commits/master)
[![coverage report](https://gitlab.com/drunkhub/drunkhub/badges/master/coverage.svg)](https://gitlab.com/drunkhub/drunkhub/-/commits/master)

## Ötlet
Egy hiánypótló alkalmazást szeretnék létrehozni leendő csapattársaimmal, ahol egy felhasználóbarát kezelőfelületen ki tudja választani, hogy melyik kocsmába menne el szívesen (természetesen miután bármelyik is kinyit).
Itt előre látja az aktuális kínálatot és az árakat. Lehet az akár egy korsó sör vagy egy kisfröccs is.
A legszimpatikusabbat kiválasztani is tudja.
#### Ez még a jövő:
- Akár kedvencek közé tenni, hogy végül törzsvendég is válhasson belőle.
 
## Tesztelés
- A tesztek eredményei a [CI/CD -> Pipelines](https://gitlab.com/Anvaroth/drunkhub/-/pipelines)  fül alatt találhatók. 

## Stage állapot elérése
- Jelenleg csak a **Master branch**-en történő változásokra fut le!
- Minden *commit* esetén lefut és deploy-olódik, ha a Client mappában vagy *.gitlab-ci.yml*-ben volt változás
- http://www.drunkhub-stage.surge.sh

## Production állapot elérése (Még nem aktív!)
- Jelenleg csak a **Master branch**-en történő változásokra fut le!
- Csak akkor fut le, ha egy **Tag**-el el van látva az adott *commit*, például *v1.0.42*
- http://www.drunkhub.surge.sh

## Arifact-ek
**(In progress)**
