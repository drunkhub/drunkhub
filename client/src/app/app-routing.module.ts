import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DrinksComponent } from './drinks/drinks.component';
import { LocationsComponent } from './locations/locations.component';
import { PubsProfileComponent } from './pubs/pubs-profile/pubs-profile.component';
import { PubsComponent } from './pubs/pubs.component';

const routes: Routes = [
  { path: '', redirectTo: 'pubs', pathMatch: 'full' },
  { path: 'pubs', component: PubsComponent },
  { path: 'pubsprofile', component: PubsProfileComponent },
  { path: 'drinks', component: DrinksComponent },
  { path: 'locations', component: LocationsComponent },
  { path: '**', component: PubsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
