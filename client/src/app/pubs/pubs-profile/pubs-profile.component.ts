import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NavigationService } from 'src/app/service/navigation.service';
import { PubTransferService } from 'src/app/service/pub-transfer.service';
import { Pub } from 'src/app/_models/models';

@Component({
  selector: 'app-pubs-profile',
  templateUrl: './pubs-profile.component.html',
  styleUrls: ['./pubs-profile.component.scss'],
})
export class PubsProfileComponent implements OnInit {
  selected: any[] = [];
  constructor(public service: PubTransferService) {}

  ngOnInit(): void {
    this.selected = this.service.getPubs();
    console.log(this.selected);
    /* this.selected = this.service.getPubs(); */
  }
}
