import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { NavigationService } from '../service/navigation.service';
import { PubTransferService } from '../service/pub-transfer.service';

@Component({
  selector: 'app-pubs',
  templateUrl: './pubs.component.html',
  styleUrls: ['./pubs.component.scss'],
})
export class PubsComponent implements OnInit {
  constructor(
    // private navigationService: NavigationService,
    private router: Router,
    public dataService: PubTransferService,
    private http: HttpClient
  ) {}

  ngOnInit(): void {
    this.http
      .get(
        'https://nominatim.openstreetmap.org/search?q=Happy+Drink+Bar&format=json'
      )
      .subscribe((tmp: any) => {
        console.log('Happy', tmp);
      });
  }

  navigate(): void {
    /* this.navigationService.setPubs(pubs); */
    this.router.navigate(['/pubsprofile']);
  }
}
