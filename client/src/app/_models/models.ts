export interface Pub {
  _id?: string;
  image_url?: string;
  name: string;
  address?: string;
  location: Location;
  stock?: number[];
  createdAt?: string;
  updatedAt?: string;
}

export interface Location {
  x: number;
  y: number;
}
