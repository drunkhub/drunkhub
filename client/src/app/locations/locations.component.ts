import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PubTransferService } from '../service/pub-transfer.service';
import { Location, Pub } from '../_models/models';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss'],
})
export class LocationsComponent implements OnInit {
  selectedLocation: Location = { x: 0, y: 0 };

  constructor(public dataService: PubTransferService) {}

  ngOnInit(): void {}

  pubLocation(pub: Pub): void {
    this.selectedLocation = pub.location;
  }
}
