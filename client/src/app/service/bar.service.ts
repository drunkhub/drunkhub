import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Pub } from '../_models/models';

@Injectable({
  providedIn: 'root',
})
export class BarService {
  uri = 'http://localhost:3000';

  constructor(private http: HttpClient) {}

  getBars(): any {
    return this.http.get<Pub[]>(`${this.uri}/bars`);
  }

  getBarById(id: string): any {
    return this.http.get(`${this.uri}/bars/${id}`);
  }

  addBar(
    name: string,
    address: string,
    location: { x: number; y: number },
    stock: number[]
  ): void {
    const bar = {
      name: ('{name}'),
      address: ('{address}'),
      location: ('{location}'),
      stock: ('{stock}'),
    };
    this.http.post(`${this.uri}/bars`, bar);
  }

  deleteBar(id: string): void {
    this.http.delete(`${this.uri}/bars/${id}`); // kiszedett return
  }
}
