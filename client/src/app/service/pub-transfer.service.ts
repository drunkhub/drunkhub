import { Injectable } from '@angular/core';
import { Pub } from '../_models/models';
import { BarService } from './bar.service';

@Injectable({
  providedIn: 'root',
})
export class PubTransferService {
  private sharedPubs: Pub[] = [];

  constructor(private bar: BarService) {}

  loadFromDatabase(): void {
    this.bar.getBars().subscribe((bars: Pub[]) => {
      this.sharedPubs = bars;
    });
  }

  getPubs(): Pub[] {
    return this.sharedPubs;
  }

  addPub(pub: Pub): void {
    this.sharedPubs.push(pub);
  }

  removePub(pub: Pub): void {
    this.sharedPubs = this.sharedPubs.filter((p) => p.name !== pub.name);
  }
}
