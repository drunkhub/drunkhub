"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var controllers_1 = require("./controllers");
require("./entities/db");
var cors = require('cors');
var app = express_1.default();
app.use(cors());
app.use(body_parser_1.default.json());
app.use(controllers_1.router);
app.listen(process.env.PORT || 3000, function () {
    console.log('Server started.');
});
module.exports = app;
