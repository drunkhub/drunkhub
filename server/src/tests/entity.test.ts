const mongooseTest = require('mongoose');
const BarModel = require('../../src/entities/bar');
const barData = {name:'Test Bar', address: 'TestAddress', location: {x: 66, y:99}, stock: [1,2] }
const DrinkModel = require('../../src/entities/drink');
const drinkData = {name:'Test Drink', description: 'Test description.', ingredients: ["A little this.", "A little that."] };
let MONGODB_URITest = process.env.PROD_MONGODB || 'mongodb+srv://admin:admin@cluster0.uknxm.mongodb.net/DrunkHUB?retryWrites=true&w=majority';

describe('Bar Entity Test:', () => {
    beforeAll(async () => {
        await mongooseTest.connect(MONGODB_URITest, { useNewUrlParser: true, useCreateIndex: true }, (err) => {
            if (err) {
                console.error(err);
                process.exit(0);
            }
        });
    });

    it('Created and saved a bar successfully.', async () => {
        const validBar = new BarModel(barData);
        const savedBar = await validBar.save();
        expect(savedBar._id).toBeDefined();
        expect(savedBar.name).toBe(barData.name);
        expect(savedBar.address).toBe(barData.address);
        await savedBar.remove();
    });

    it('Inserted a bar successfully, but the fields not in the schema are undefined.', async () => {
        const barWithInvalidField = new BarModel({name:'Test Bar', address: 'TestAddress', location: {x: 66, y:99}, stock: [1,2]});
        const savedBarWithInvalidField = await barWithInvalidField.save();
        expect(savedBarWithInvalidField._id).toBeDefined();
        expect(savedBarWithInvalidField.adddress).toBeUndefined();
        await savedBarWithInvalidField.remove();
    });

    it('Created a bar without a required field, got expected error.', async () => {
        const barWithoutRequiredField = new BarModel({ name: 'TekLoon' });
        let err;
        try {
            const savedBarWithoutRequiredField = await barWithoutRequiredField.save();
            err = savedBarWithoutRequiredField;
        } catch (e) {
            err = e;
        }
        expect(err).toBeInstanceOf(mongooseTest.Error.ValidationError)
        expect(err.errors.address).toBeDefined();
        await barWithoutRequiredField.remove();
    });

    afterAll(async () => {
        await mongooseTest.connection.close()
    })
})

describe('Drink Entity Test:', () => {
    beforeAll(async () => {
        await mongooseTest.connect(MONGODB_URITest, { useNewUrlParser: true, useCreateIndex: true }, (err) => {
            if (err) {
                console.error(err);
                process.exit(0);
            }
        });
    });

    it('Created and saved a drink successfully.', async () => {
        const validDrink = new DrinkModel(drinkData);
        const savedDrink = await validDrink.save();
        expect(savedDrink._id).toBeDefined();
        expect(savedDrink.name).toBe(drinkData.name);
        expect(savedDrink.description).toBe(drinkData.description);
        await savedDrink.remove();
    });

    it('Inserted a drink successfully, but the fields not in the schema are undefined.', async () => {
        const drinkWithInvalidField = new DrinkModel({name:'Test Drink', flavor: "Bad.", description: 'Test description.', ingredients: ["A little this.", "A little that."], });
        const savedDrinkWithInvalidField = await drinkWithInvalidField.save();
        expect(savedDrinkWithInvalidField._id).toBeDefined();
        expect(savedDrinkWithInvalidField.flavor).toBeUndefined();
        await savedDrinkWithInvalidField.remove();
    });

    it('Created a drink without a required field, got expected error.', async () => {
        const drinkWithoutRequiredField = new DrinkModel({ name: 'The BEST Drink' });
        let err;
        try {
            const savedDrinkWithoutRequiredField = await drinkWithoutRequiredField.save();
            err = savedDrinkWithoutRequiredField;
        } catch (e) {
            err = e;
        }
        expect(err).toBeInstanceOf(mongooseTest.Error.ValidationError)
        expect(err.errors.description).toBeDefined();
        await drinkWithoutRequiredField.remove();
    });

    afterAll(async () => {
        await mongooseTest.connection.close()
    })
})
