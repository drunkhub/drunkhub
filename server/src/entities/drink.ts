const mongooseDrink = require('mongoose')
const Schema = mongooseDrink.Schema

const Drink = new Schema(
    {
        name: { type: String, required: true },
        description: { type: String, required: true },
        ingredients: { type: [String], required: true},
    },
    { timestamps: true },
)

module.exports = mongooseDrink.model('drinks', Drink);