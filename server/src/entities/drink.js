"use strict";
var mongooseDrink = require('mongoose');
var Schema = mongooseDrink.Schema;
var Drink = new Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    ingredients: { type: [String], required: true },
}, { timestamps: true });
module.exports = mongooseDrink.model('drinks', Drink);
