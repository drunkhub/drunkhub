import * as mongoose from 'mongoose';

const drinkSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    ingredients: {
      type: [String],
      required: true,
    },
  },
  { timestamps: true }
);

mongoose.model("Drink", drinkSchema);
