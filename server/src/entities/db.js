"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var MONGODB_URI = "mongodb+srv://admin:admin@cluster0.uknxm.mongodb.net/DrunkHUB?retryWrites=true&w=majority";
mongoose.connect(MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
});
// connection events
mongoose.connection.on("connected", function () {
    console.log("Mongoose connected to " + MONGODB_URI);
});
mongoose.connection.on("error", function (err) {
    console.log("Mongoose connection error: " + err);
});
mongoose.connection.on("disconnected", function () {
    console.log("Mongoose disconnected");
});
// SCHEMA IMPORTS
require("./bar.schema");
require("./drink.schema");
