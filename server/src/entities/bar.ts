const mongooseBar = require('mongoose')
const SchemaBar = mongooseBar.Schema

const Bar = new SchemaBar(
    {
        name: { type: String, required: true},
        address: { type: String, required: true},
        location: { x: {type: Number, required: true} , y: {type: Number, required: true} },
        stock: { type: [Number], required: true}
    },
    { timestamps: true },
)

module.exports = mongooseBar.model('bars', Bar)
