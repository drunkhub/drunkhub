const mongoose = require("mongoose");
const MONGODB_URI = "mongodb+srv://admin:admin@cluster0.uknxm.mongodb.net/DrunkHUB?retryWrites=true&w=majority";

mongoose.connect(MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

// connection events
mongoose.connection.on("connected", function () {
    console.log("Mongoose connected to " + MONGODB_URI);
  });
  
  mongoose.connection.on("error", function (err: any) {
    console.log("Mongoose connection error: " + err);
  });
  
  mongoose.connection.on("disconnected", function () {
    console.log("Mongoose disconnected");
  });

// SCHEMA IMPORTS
import './bar.schema';
import './drink.schema';