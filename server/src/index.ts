import express from 'express';
import bodyParser from 'body-parser';
import { router } from './controllers';

import './entities/db';

const cors = require('cors');

const app = express();

app.use(cors())

app.use(bodyParser.json());

app.use(router);

app.listen(process.env.PORT || 3000, () => {
    console.log('Server started.');
});

module.exports = app;