"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.barRouter = void 0;
var mongoose = __importStar(require("mongoose"));
require("../entities/db");
var express_1 = require("express");
exports.barRouter = express_1.Router();
var Bar = mongoose.model("Bar");
// Create and save new bar
exports.barRouter
    .post("", function (req, res) {
    // Create a new bar
    var bar = new Bar({
        name: req.body.name,
        address: req.body.address,
        location: req.body.location,
        stock: req.body.stock,
    });
    // Save bar in the db
    bar
        .save()
        .then(function (data) {
        res.send(data);
    })
        .catch(function (err) {
        res.status(500).send({
            message: err.message || "Some error occured while creating the bar.",
        });
    });
})
    // Find all bars in the db
    .get("", function (req, res) {
    Bar.find()
        .then(function (data) {
        res.send(data);
    })
        .catch(function (err) {
        res.status(500).send({
            message: err.message || "Some error occured while retrieving bars.",
        });
    });
})
    // Find bar by id
    .get("/:id", function (req, res) {
    var id = req.params.id;
    Bar.findById(id)
        .then(function (data) {
        if (!data) {
            res.status(404).send({ message: "Not found bar with id " + id });
        }
        else {
            res.send(data);
        }
    })
        .catch(function () {
        res.status(500).send({ message: "Error retrieving bar with id=" + id });
    });
})
    // Update bar by id
    .put("/:id", function (req, res) {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!",
        });
    }
    var id = req.params.id;
    Bar.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(function (data) {
        if (!data) {
            res.status(404).send({
                message: "Cannot update bar with id=" + id + ". Maybe bar was not found!",
            });
        }
        else
            res.send({ message: "bar was updated succesfully." });
    })
        .catch(function () {
        res.status(500).send({
            message: "Error updating bar with id=" + id,
        });
    });
})
    // Delete bar by id
    .delete("/:id", function (req, res) {
    var id = req.params.id;
    Bar.findByIdAndRemove(id)
        .then(function (data) {
        if (!data) {
            res.status(404).send({
                message: "Cannot delete bar with id=" + id + ". Maybe bar was not found!",
            });
        }
        else {
            res.send({
                message: "Bar was deleted successfully!",
            });
        }
    })
        .catch(function () {
        res.status(500).send({
            message: "Could not delete bar with id=" + id,
        });
    });
});
