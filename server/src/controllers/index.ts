import { Router } from 'express';
import { barRouter } from './bar.controller';
import { drinkRouter } from './drink.controller';

export const router = Router();

router.use('/bars', barRouter);
router.use('/drinks', drinkRouter);