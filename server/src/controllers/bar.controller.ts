import * as mongoose from "mongoose";
import "../entities/db";
import { Router } from "express";

export const barRouter = Router();

const Bar = mongoose.model("Bar");

// Create and save new bar
barRouter
  .post("", (req, res) => {
    // Create a new bar
    const bar = new Bar({
      name: req.body.name,
      address: req.body.address,
      location: req.body.location,
      stock: req.body.stock,
    });

    // Save bar in the db
    bar
      .save()
      .then((data) => {
        res.send(data);
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "Some error occured while creating the bar.",
        });
      });
  })

  // Find all bars in the db
  .get("", (req, res) => {
    Bar.find()
      .then((data) => {
        res.send(data);
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "Some error occured while retrieving bars.",
        });
      });
  })

  // Find bar by id
  .get("/:id", (req, res) => {
    const id = req.params.id;

    Bar.findById(id)
      .then((data) => {
        if (!data) {
          res.status(404).send({ message: "Not found bar with id " + id });
        } else {
          res.send(data);
        }
      })
      .catch(() => {
        res.status(500).send({ message: "Error retrieving bar with id=" + id });
      });
  })

  // Update bar by id
  .put("/:id", (req, res) => {
    if (!req.body) {
      return res.status(400).send({
        message: "Data to update can not be empty!",
      });
    }

    const id = req.params.id;

    Bar.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
      .then((data) => {
        if (!data) {
          res.status(404).send({
            message: `Cannot update bar with id=${id}. Maybe bar was not found!`,
          });
        } else res.send({ message: "bar was updated succesfully." });
      })
      .catch(() => {
        res.status(500).send({
          message: "Error updating bar with id=" + id,
        });
      });
  })

  // Delete bar by id
  .delete("/:id", (req, res) => {
    const id = req.params.id;

    Bar.findByIdAndRemove(id)
      .then((data) => {
        if (!data) {
          res.status(404).send({
            message: `Cannot delete bar with id=${id}. Maybe bar was not found!`,
          });
        } else {
          res.send({
            message: "Bar was deleted successfully!",
          });
        }
      })
      .catch(() => {
        res.status(500).send({
          message: "Could not delete bar with id=" + id,
        });
      });
  });
